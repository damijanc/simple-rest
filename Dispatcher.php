<?php

/*
 * Dispatcher
 *
 * From the array of dynamic URLs it calls specified classes.
 *
 */

class Dispatcher {

  private $match_arr = null;

  /*
   * Constructor accepts
   *
   * URL <---> classs mapping like:
   *
   * $url_mapping = array(
   *     "/rest/fti/foo?.*" => array('/rest/archive/fti.class.php', 'FTI', 'foo'),
   *     "/rest/fti/article/url/(.*)/(.*)" => array('/rest/archive/fti.class.php', 'FTI', 'article'),
   * );
   *
   * When HTTP request is made for /rest/* URL the call is dispatched to
   * the class and function specified in the array.
   */

  public function __construct ($url_mapping) {
    $this->url_mapping = $url_mapping;
  }

  /*
   * Properly parse URL parameters (CGI variant).
   * @returns: array of URL parameters.
   */

  function parse_url_params ($str) {
    // Result array.
    $arr = array();

    // Parse URL parameters.
    $pairs = explode('&', $str);
    foreach ($pairs as $pair) {

      $exp_array = explode('=', $pair, 2);
      if (count($exp_array) == 2)
        list($name, $value) = $exp_array;
      else
        continue;

      // Decode HTML entities.
      $value = urldecode($value);

      // If key already exists create an array of all the values.
      if (isset($arr[$name])) {
        // Put multiple values into an array.
        if (is_array($arr[$name])) {
          $arr[$name][] = $value;
        }
        else {
          $arr[$name] = array($arr[$name], $value);
        }
      }
      // Otherwise only a simple value is stored.
      else {
        $arr[$name] = $value;
      }
    }

    return $arr;
  }

  /*
   * Internal buffer callback.
   */

  static function ob_callback ($buffer) {
    return $buffer;
  }

  /*
   * Run the dispatcher.
   */

  public function run () {
    // Match the request URL agaings the URL mapping.
    $matches = array();
    $is_match = false;
    foreach ($this->url_mapping as $match_str => $match_arr) {
      $match_str = sprintf("/%s/", str_replace("/", "\/", $match_str));
      $is_match = preg_match($match_str, $_SERVER['REQUEST_URI'], $matches);
      if ($is_match)
        break;
    }

    // Bad request (400) if no match.
    if (!$is_match) {
      header("HTTP/1.0 400 Bad Request");
      return;
    }

    // Prepare the matches found.
    // (more than 1, for the whole string is always the first match)
    $rest_arr = array();
    if ($is_match && count($matches) > 1) {
      $rest_arr = array_slice($matches, 1);
    }

    // Parse query strings/
    $query_params = array();
    /*if ($_SERVER['QUERY_STRING']) {
      $query_params = $this->parse_url_params($_SERVER['QUERY_STRING']);
    }*/

    $query_params = $_REQUEST;

    list($lib_name, $class_name, $function_name) = $match_arr;
    $lib_name = __DOCROOT__ . $lib_name;

    if (file_exists($lib_name))
      require_once($lib_name);
    else
      throw new Exception("File $lib_name does not exist.");

    $dis_obj = new $class_name();
    if ($dis_obj) {
      ob_clean();
      ob_start("Dispatcher::ob_callback");
      $dis_obj->$function_name($rest_arr, $query_params);
      if (!ob_get_length() === false)
        ob_flush();
    }
  }

}

?>