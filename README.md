Simple implementation of REST:

```
URL <---> class mapping like:
   
    $url_mapping = array(
        "/rest/fti/foo?.*" => array('/rest/archive/fti.class.php', 'FTI', 'foo'),
        "/rest/fti/article/url/(.*)/(.*)" => array('/rest/archive/fti.class.php', 'FTI', 'article'),
    );

```
   
When HTTP request is made for /rest/* URL the call is dispatched to
the class and function specified in the array.

This should be implemented on server level